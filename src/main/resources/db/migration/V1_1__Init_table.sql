CREATE SCHEMA IF NOT EXISTS employees;

CREATE TABLE USERS
(
    ID         CHAR(36) PRIMARY KEY NOT NULL,
    NAME       TEXT                 NOT NULL,
    AGE        INT                  NOT NULL,

    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone
);

CREATE TABLE ROLES
(
    ID         CHAR(36) PRIMARY KEY NOT NULL,
    NAME       TEXT                 NOT NULL,
    USER_ID    CHAR(36)              NULL ,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone,
    FOREIGN KEY (USER_ID) REFERENCES USERS(ID) ON DELETE CASCADE
);



