package com.testos.repository;

import com.testos.entity.RoleEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface IRoleRepository extends JpaRepository<RoleEntity, UUID> {
    RoleEntity findByName(String roleName);
}
