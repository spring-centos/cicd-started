package com.testos.entity;

import com.testos.entity.base.BaseEntity;
import lombok.*;

import javax.persistence.*;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "USERS")
public class UserEntity extends BaseEntity {
    @Column(name = "name")
    private String name;

    @Column(name = "age")
    private int age;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "userEntity")
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private List<RoleEntity> roleEntities;
}
