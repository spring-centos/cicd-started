package com.testos.service.user;

import com.testos.dto.user.UserRequestDto;
import com.testos.dto.user.UserResponseDto;
import com.testos.entity.RoleEntity;
import com.testos.entity.UserEntity;
import com.testos.handler_exception.RestError;
import com.testos.handler_exception.SingleErrorException;
import com.testos.mapper.IUserMapper;
import com.testos.repository.IUserRepository;
import com.testos.service.role.IRoleService;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserService implements IUserService {
    private final IUserRepository repository;
    private final IUserMapper userMapper;

    private final IRoleService roleService;

    public UserService(IUserRepository repository, IUserMapper userMapper, IRoleService roleService) {
        this.repository = repository;
        this.userMapper = userMapper;
        this.roleService = roleService;
    }

    @Override
    public UserEntity create(UserRequestDto userRequestDto) {
        if (userRequestDto.getRoleNames().isEmpty()) {
            RestError err = RestError.builder()
                    .message("Roles not nut")
                    .resource(RoleEntity.class.toString())
                    .build();
            throw new SingleErrorException(err, HttpStatus.BAD_REQUEST);
        }

        List<RoleEntity> roleEntities = new ArrayList<>();

        for (String roleName : userRequestDto.getRoleNames()) {
            if (roleService.findByName(roleName) != null) {
                roleEntities.add(roleService.findByName(roleName));
            }
        }

        UserEntity userEntity = UserEntity.builder()
                .name(userRequestDto.getName())
                .age(userRequestDto.getAge())
                .roleEntities(roleEntities)
                .build();

        return repository.save(userEntity);
    }

    @Override
    public List<UserResponseDto> findAll() {
        return userMapper.toUserResponseDtos(repository.findAll());
    }
}
