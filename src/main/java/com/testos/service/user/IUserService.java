package com.testos.service.user;

import com.testos.dto.user.UserRequestDto;
import com.testos.dto.user.UserResponseDto;
import com.testos.entity.UserEntity;

import java.util.List;

public interface IUserService {
    UserEntity create(UserRequestDto userRequestDto);

    List<UserResponseDto> findAll();
}
