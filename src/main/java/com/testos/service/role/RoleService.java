package com.testos.service.role;

import com.testos.dto.role.RoleRequestDto;
import com.testos.dto.role.RoleResponseDto;
import com.testos.entity.RoleEntity;
import com.testos.handler_exception.RestError;
import com.testos.handler_exception.SingleErrorException;
import com.testos.mapper.IRoleMapper;
import com.testos.repository.IRoleRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoleService implements IRoleService {
    private final IRoleRepository repository;
    private final IRoleMapper roleMapper;

    public RoleService(IRoleRepository repository, IRoleMapper roleMapper) {
        this.repository = repository;
        this.roleMapper = roleMapper;
    }

    @Override
    public RoleEntity findByName(String roleName) {
        if (repository.findByName(roleName) == null) {
            RestError err = RestError.builder()
                    .message("Role is not existed")
                    .resource(RoleEntity.class.toString())
                    .build();
            throw new SingleErrorException(err, HttpStatus.NOT_FOUND);
        }
        return repository.findByName(roleName);
    }

    @Override
    public List<RoleResponseDto> findAll() {
        return roleMapper.toRoleResponseDtos(repository.findAll());
    }

    @Override
    public RoleEntity create(RoleRequestDto roleRequestDto) {
        return repository.save(roleMapper.toRoleEntity(roleRequestDto));
    }
}
