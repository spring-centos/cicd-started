package com.testos.service.role;

import com.testos.dto.role.RoleRequestDto;
import com.testos.dto.role.RoleResponseDto;
import com.testos.entity.RoleEntity;

import java.util.List;

public interface IRoleService {
    RoleEntity findByName(String roleName);

    List<RoleResponseDto> findAll();

    RoleEntity create(RoleRequestDto roleRequestDto);
}
