package com.testos.mapper;

import com.testos.dto.user.UserRequestDto;
import com.testos.dto.user.UserResponseDto;
import com.testos.entity.UserEntity;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface IUserMapper {
    UserEntity toUserEntity(UserRequestDto userRequestDto);

    UserResponseDto toUserResponseDto(UserEntity userEntity);

    List<UserEntity> toUserEntities(List<UserRequestDto> userRequestDtos);

    List<UserResponseDto> toUserResponseDtos(List<UserEntity> userEntities);
}
