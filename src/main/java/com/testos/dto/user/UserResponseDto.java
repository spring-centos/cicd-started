package com.testos.dto.user;

import com.testos.dto.base.BaseDto;
import com.testos.dto.role.RoleResponseDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserResponseDto extends BaseDto {
    private String name;
    private int age;
    private List<RoleResponseDto> roleEntities;
}
