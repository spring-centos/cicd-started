package com.testos.dto.role;

import com.testos.dto.base.BaseDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RoleResponseDto extends BaseDto {
    @Column(name = "name")
    private String name;
}
