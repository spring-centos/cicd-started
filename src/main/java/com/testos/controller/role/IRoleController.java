package com.testos.controller.role;

import com.testos.common.ApiUtils;
import com.testos.dto.role.RoleRequestDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Api(tags = "Role")
@RequestMapping(value = ApiUtils.Role, produces = MediaType.APPLICATION_JSON_VALUE)
public interface IRoleController {
    // ---------------
    // Create new role
    // ---------------
    @ApiOperation(value = "API to create new role")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Post Successfully")})
    @PostMapping
    ResponseEntity create(@RequestBody RoleRequestDto roleRequestDto);

    // ---------------
    // Get list role
    // ---------------
    @ApiOperation(value = "API to get list role")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Get Successfully")})
    @GetMapping
    ResponseEntity getListRole();
}
