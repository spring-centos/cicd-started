package com.testos.controller.role;

import com.testos.dto.role.RoleRequestDto;
import com.testos.service.role.IRoleService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RoleController implements IRoleController {
    private final IRoleService roleService;

    public RoleController(IRoleService roleService) {
        this.roleService = roleService;
    }

    @Override
    public ResponseEntity create(RoleRequestDto roleRequestDto) {
        return new ResponseEntity(roleService.create(roleRequestDto), HttpStatus.OK);
    }

    @Override
    public ResponseEntity getListRole() {
        return new ResponseEntity((roleService.findAll()), HttpStatus.OK);
    }
}
