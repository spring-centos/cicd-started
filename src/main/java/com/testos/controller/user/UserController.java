package com.testos.controller.user;

import com.testos.dto.user.UserRequestDto;
import com.testos.service.user.IUserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController implements IUserController {
    private final IUserService userService;

    public UserController(IUserService userService) {
        this.userService = userService;
    }

    @Override
    public ResponseEntity registerUser(UserRequestDto userRequestDto) {
        return new ResponseEntity(userService.create(userRequestDto), HttpStatus.OK);
    }

    @Override
    public ResponseEntity getListUser() {
        return new ResponseEntity((userService.findAll()), HttpStatus.OK);
    }
}
