package com.testos.controller.user;

import com.testos.common.ApiUtils;
import com.testos.dto.user.UserRequestDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Api(tags = "User")
@RequestMapping(value = ApiUtils.USER, produces = MediaType.APPLICATION_JSON_VALUE)
public interface IUserController {
    // ---------------
    // Register USer
    // ---------------
    @ApiOperation(value = "API to register user")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Post SuccessFully")})
    @PostMapping("/register")
    ResponseEntity registerUser(@RequestBody UserRequestDto userRequestDto);

    // ---------------
    // Get list user
    // ---------------
    @ApiOperation(value = "API to get list user")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Get SuccessFully")})
    @GetMapping
    ResponseEntity getListUser();

}
