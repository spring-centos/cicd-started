package com.testos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestosApplication {

    public static void main(String[] args) {
        SpringApplication.run(TestosApplication.class, args);
    }

}
