#FROM maven:3.8.1-jdk-11-slim AS MAVEN_BUILD
#
#COPY pom.xml /build/
#COPY src /build/src/
#WORKDIR /build/
#
#RUN mvn clean install -DskipTests=true

FROM adoptopenjdk/openjdk11:alpine-jre

VOLUME /tmp

#COPY --from=MAVEN_BUILD /build/target/testos-0.0.1-SNAPSHOT.jar testos-api.jar
ADD /target/testos-0.0.1-SNAPSHOT.jar testos-api.jar

ENTRYPOINT ["java","-jar","testos-api.jar"]